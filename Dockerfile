FROM python:3.7-alpine3.8
LABEL author="Justin Millette"
COPY * /app/
RUN pip3 install Flask
EXPOSE 8081
ARG DISPLAY_COLOR=green
ARG DISPLAY_FONT=arial
ARG ENVIRONMENT=Prod
HEALTHCHECK CMD curl --fail http://localhost:8081/ || exit 1
RUN adduser -D -g '' justin
USER justin
CMD python /app/app.py
